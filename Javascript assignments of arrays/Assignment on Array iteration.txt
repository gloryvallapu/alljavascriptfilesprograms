****************VLR Trainings***********************
------------------------------------------------------

Assignments On Array of Objects
example
---------
var products = [
{
"name": "Duracell - AAA Batteries (4-Pack)",
"type": "HardGood",
"price": 5.49,
"category": "Household Batteries",
"manufacturer": "Duracell",
},
{
"name": "Hard Rock TrackPak - Mac",
"type": "Software",
"price": 29.99,
"category": "Recording Equipment",
"manufacturer": "Hal Leonard",
},
{
"name": "Duracell - AA 1.5V CopperTop Batteries (4-Pack)",
"type": "HardGood",
"price": 5.62,
"category": "Household Batteries",
"manufacturer": "Duracell",
},
{
"name": "Energizer - MAX Batteries AA (4-Pack)",
"type": "HardGood",
"price": 5.32,
"category": "Household Batteries",
"manufacturer": "Energizer",
},
{
"name": "METRA - Antenna Cable Adapter - Black",
"type": "HardGood",
"price": 13.99,
"category": "Antennas & Adapters",
"manufacturer": "Metra",
},
{
"name": "WipeDrive Six - Mac|Windows",
"type": "Software",
"price": 23.99,
"category": "Maintenance Software",
"manufacturer": "White Canyon",
}
];
1. Print all the product names.
2. Print all the hardgoods.
3. Print all the softwares.
4. Print all the categories.
5. Print only the products manufactured by Duracell.
6. Print the product names in ascending order of their prices.
7. Print only those products whose price is more than 14.99.
8. Print only those products whose price is less than 9.99.
9. Print the total price of all the hardgoods.
10. Print the average price of the softwares
------------------------------------------
Assignment on Array iteration
[12,23,46,-19,30,398,-98,37,10,30,97,5,30,11]
a)print all the Array values using for loop.
b) print all numbers in an array except the first element.
c) Print all elements in an array except the last element
d) Print all the numbers from last index to first index
e) Print all the numbers from last index to first index except the first element
f) Print all the numbers from last index to first index except the last element
g)print the minimum number in the Array.
h)print the maximum number in the array.
i)sum of all the numbers in the Array.
j)Print the average of the numbers in the Array.
k)print values that are greater than 30.
l)print all the even numbers in the Array.
m)print all the odd numbers in the Array.
n)print all the positive numbers in the array.
o)search whether 10 is there in the array or not.
p)print total number of times the number 30 is present in the Array.
q)print index of the number 46 in the Array.
r)Write a program to store the array into another array.
s)Print the 3-digited number in the array.
t)Print only multiples of 2 AND 3 from an array.
---------------------------------------------------
clear trial technologies
----------------------------
Write a function to remove a value from array?
Write a function to get Max value of a number Array?
Write a function to find indexes of value from a sorted array.
Write a function to find average marks by subject. Given Data is in following format.
data = [{
id: "Unique String Value",
subject: "Any String Value",
marks: number value
}]
Write a method to check whether 2 strings are made up of same characters
Write a program to render customer list in a table. With Each row having a remove button in Angular
How to monitor memory, CPU of Web Application?
If your Page is crashing due to high memory and high CPU Usage than how you will resolve?
Write a function to sort date and time column in a table
Write a method to reverse an Array of n integers
